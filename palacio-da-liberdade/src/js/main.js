$(window).load(function() {
      var windowWidth = $(window).width();
        if(windowWidth > 768){
          skrollr.init({
            smoothScrolling: true,
            forceHeight: false,
          });
      };

    $(".element-wrapper").hover(function(){
      $(this).find(".profile-infos").addClass("open");
      $(this).find(".social-feed-text").addClass("open");
    }, 

    function(){
      $(this).find(".profile-infos").removeClass("open");
      $(this).find(".social-feed-text").removeClass("open");
    });
});


$(function() {

	$('#fullpage').fullpage({ 
	  autoScrolling: false,
	  verticalCentered: false,
	  scrollBar: false,
	});

	$('.p-escadaria, .p-piano, .p-galeria, .p-cartao-postal, .p-visitacao-destaque').parallax({
      speed : 0.55
    });

	$('.social-feed-container').socialfeed({
	  // INSTAGRAM
	  instagram:{
	      accounts: ['#palaciodaliberdade'],
	      limit:12,
	      client_id: '3e25f64814fa41bfa5749cd952dfd039'
	  },
	  // GENERAL SETTINGS
	  length:100,
	  show_media:true,
	  // Moderation function - if returns false, template will have class hidden
	  moderation: function(content){
	      return  (content.text) ? content.text.indexOf('fuck') == -1 : true;
	  },
	  //update_period: 5000,
	  // When all the posts are collected and displayed - this function is evoked
	  callback: function(){
	      console.log('all posts are collected');
	  }
	});
    
    var image_src = {

                r1: "background-position: 0 0%; background-size: 100%;",
                r2: "background-position: 0 5.263158%; background-size: 100%;",
                r3: "background-position: 0 10.526316%; background-size: 100%;",
                r4: "background-position: 0 15.789474%; background-size: 100%;",
                r5: "background-position: 0 21.052632%; background-size: 100%;",
                r6: "background-position: 0 26.315789%; background-size: 100%;",
                r7: "background-position: 0 31.578947%; background-size: 100%;",
                r8: "background-position: 0 36.842105%; background-size: 100%;",
                r9: "background-position: 0 42.105263%; background-size: 100%;",
                r10: "background-position: 0 47.368421%; background-size: 100%;",
                r11: "background-position: 0 52.631579%; background-size: 100%;",
                r12: "background-position: 0 57.894737%; background-size: 100%;",
                r13: "background-position: 0 63.157895%; background-size: 100%;",
                r14: "background-position: 0 68.421053%; background-size: 100%;",
                r15: "background-position: 0 73.684211%; background-size: 100%;",
                r16: "background-position: 0 78.947368%; background-size: 100%;",
                r17: "background-position: 0 84.210526%; background-size: 100%;",
                r18: "background-position: 0 89.473684%; background-size: 100%;",
                r19: "background-position: 0 94.736842%; background-size: 100%;",
                r20: "background-position: 0 100%; background-size: 100%;",

            };

    var larguraAtual = $(document).width() / 20;

    console.log(larguraAtual);

    $(document).mousemove(function (event) {
        var mouse = {
            x: event.pageX,
        };

        if (mouse.x < larguraAtual) {
            $(".image_rotate").attr("style", image_src.r1);

        } else if (mouse.x > larguraAtual && mouse.x < larguraAtual * 2) {
            $(".image_rotate").attr("style", image_src.r2);

        } else if (mouse.x > larguraAtual * 2 && mouse.x < larguraAtual * 3) {
            $(".image_rotate").attr("style", image_src.r3);

        } else if (mouse.x > larguraAtual * 3 && mouse.x < larguraAtual * 4) {
            $(".image_rotate").attr("style", image_src.r4);

        } else if (mouse.x > larguraAtual * 4 && mouse.x < larguraAtual * 5) {
            $(".image_rotate").attr("style", image_src.r5);

        } else if (mouse.x > larguraAtual * 5 && mouse.x < larguraAtual * 6) {
            $(".image_rotate").attr("style", image_src.r6);

        } else if (mouse.x > larguraAtual * 6 && mouse.x < larguraAtual * 7) {
            $(".image_rotate").attr("style", image_src.r7);

        } else if (mouse.x > larguraAtual * 7 && mouse.x < larguraAtual * 8) {
            $(".image_rotate").attr("style", image_src.r8);

        } else if (mouse.x > larguraAtual * 8 && mouse.x < larguraAtual * 9) {
            $(".image_rotate").attr("style", image_src.r9);

        } else if (mouse.x > larguraAtual * 9 && mouse.x < larguraAtual * 10) {
            $(".image_rotate").attr("style", image_src.r10);

        } else if (mouse.x > larguraAtual * 10 && mouse.x < larguraAtual * 11) {
            $(".image_rotate").attr("style", image_src.r11);

        } else if (mouse.x > larguraAtual * 11 && mouse.x < larguraAtual * 12) {
            $(".image_rotate").attr("style", image_src.r12);

        } else if (mouse.x > larguraAtual * 12 && mouse.x < larguraAtual * 13) {
            $(".image_rotate").attr("style", image_src.r13);

        } else if (mouse.x > larguraAtual * 13 && mouse.x < larguraAtual * 14) {
            $(".image_rotate").attr("style", image_src.r14);

        } else if (mouse.x > larguraAtual * 14 && mouse.x < larguraAtual * 15) {
            $(".image_rotate").attr("style", image_src.r15);

        } else if (mouse.x > larguraAtual * 15 && mouse.x < larguraAtual * 16) {
            $(".image_rotate").attr("style", image_src.r16);

        } else if (mouse.x > larguraAtual * 16 && mouse.x < larguraAtual * 17) {
            $(".image_rotate").attr("style", image_src.r17);

        } else if (mouse.x > larguraAtual * 17 && mouse.x < larguraAtual * 18) {
            $(".image_rotate").attr("style", image_src.r18);

        } else if (mouse.x > larguraAtual * 18 && mouse.x < larguraAtual * 19) {
            $(".image_rotate").attr("style", image_src.r19);

        } else if (mouse.x > larguraAtual * 19 && mouse.x < larguraAtual * 20) {
            $(".image_rotate").attr("style", image_src.r20);
        }

    });


    $('.btn-andar-2').click(function() {

        $('.andar-1').removeClass('show').addClass('no-show');
        $('.lista-andar-1').removeClass('show').addClass('no-show');

        $('.andar-2').removeClass('no-show').addClass('show');
        $('.lista-andar-2').removeClass('no-show').addClass('show');


    });

    $('.btn-andar-1').click(function() {

        $('.andar-2').removeClass('show').addClass('no-show');
        $('.lista-andar-2').removeClass('show').addClass('no-show');

        $('.andar-1').removeClass('no-show').addClass('show');
        $('.lista-andar-1').removeClass('no-show').addClass('show');

    });

});
