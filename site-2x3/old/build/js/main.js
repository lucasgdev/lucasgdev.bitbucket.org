$(function() {

	//Script para efeito smooth scroll usando ID

		$('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body,.scroller').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
	  });

	//Script Menu
	
	new mlPushMenu( document.getElementById( 'mp-menu' ), 
	document.getElementById( 'trigger' ) );

	//Script start Animações

	var wow = new WOW (
		{
			scrollContainer: '.scroller'
		}
	)
	wow.init();

	// Script Soluções Home

	$('.js-open-solution, .js-close-solutions').on('click', function(){
    	$('.solutions').toggleClass('solutions-open');
    });

    //Script Contato Box

    $('.item-contato').click(function(){
	     if ( $( ".fullscreen_show" ).is( ":hidden" ) ) {
	       $( ".fullscreen_show" ).slideDown( "slow" );
	     }
    });

    $('.contato-close').click(function() {
      $('.fullscreen_show').css('display','none');
    });
	

});
